var express = require('express');
var con = require('./db');
var Promise = require('promise');
var formidable = require('formidable');
const mongoose = require('mongoose');
var body = require('body-parser');
var _ = require('underscore-node');
var Q = require("q");
var FCM = require('fcm-node');
// var fcm = new FCM('AAAASGUfVDw:APA91bEW_8s4YNhx2XXHOs7EFIR8V_cJe2oYD3MRhQjtt3s5LSq6KhylVjnfPTJE8vZw8mvSaGPG3X-0OTXTDl1zVQQ_zJ1SVm8GLUQB5daVU8nEypiNJXR2VVn6PorQDvja4qTOwla0'); ///client server key
// var fcm2 = new FCM('AAAAzgOun5w:APA91bH5VC_dllp41Z9CwaEHSH4OAJlgZvDzQi2nMYypK_0l0wtGhFpAXRAgCwMMnlSkoSNmGVkFqVCG2AiiW2vyj5pgjCbBOkQmx-UjmSeFCX-hsuf6orsyFuEapMcgLLM4DlBh7xxN'); ///Photographer server key
var http = require('http');
var router = express.Router();
var nodemailer = require('nodemailer');
var Genpassword = require('secure-random-password');
var User = require("../models/user_model");
var Activity = require("../models/activity_model");
var CreatedActivity = require("../models/createdactivity_model");
var ActivityPoint = require("../models/activitypoint_model");
//var Notification = require("../models/notification");
var crypto = require('crypto');
var multer = require('multer');
var extend = require('node.extend');
var async = require("async");
var $ = require("jquery");
// var client_profile_image = 'https://s3.us-east-2.amazonaws.com/keyfi/clientprofileimages/'
// var upload_images_url = 'https://s3.us-east-2.amazonaws.com/keyfi/images/';
var AWS = require('aws-sdk');
var fs = require('fs');
var path = require('path');
var multerS3 = require('multer-s3');
var unique = require('array-unique');
var str = require('node-strings');
const uniqueArrayBy = require('unique-array-by')
var md5 = require('md5');
var apn = require('apn');
var db;
const ObjectId = require("mongodb").ObjectID;

/*multer for storing originalname of files in database and also in storage*/
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/images/');
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});
/*multer for storing fieldname + datetime of files NOT in database and also in storage*/
// SET STORAGE
// var storage1 = multer.diskStorage({
//   destination: function (req, file, cb) {
//     cb(null,'./uploads/images/');
//   },
//   filename: function (req, file, cb) {
//     cb(null, file.fieldname + '-' + Date.now())
//   }
// })

var upload = multer({ storage: storage })
var upload_profile_image = multer({ storage: storage, limits: { fileSize: 1024 * 1024 * 50 } });
var upload_image = multer({ storage: storage, limits: { fileSize: 1024 * 1024 * 50 } });
var upload = multer({ storage: storage, limits: { fileSize: 1024 * 1024 * 50 } });
/*base url for getting images of a user*/
// var profile_images = 'http://localhost/leaderboard_nodejs/uploads/images/';
// var baseurl = 'http://localhost/leaderboard_nodejs/uploads/default.png';
/*if user havnt a profile pic then it will be default picture*/
var profile_images = 'https://93.188.167.68/projects/leaderboard_nodejs/uploads/images/';
var baseurl = 'https://93.188.167.68/projects/leaderboard_nodejs/uploads/default.png';

/*-------------------------functions-------------------------*/


//Register API
router.post('/register', upload.single('image'), (req, res, next) => {
  var user_name = req.body.user_name;
  var email = req.body.email.toLowerCase();
  var password = req.body.password;
  var pwd = md5(password);
  var c_date = req.body.date;
  var m_date = req.body.date;
  var phone = req.body.phone_no;
  var random_num = Math.floor(Math.random() * 1000000) + 1;
  var de_token = random_num.toString();
  var mykey = crypto.createCipher('aes-128-cbc', 'token');
  var token_en = mykey.update(de_token, 'utf8', 'hex') + mykey.final('hex');
  if (req.body.user_name == "" || req.body.email == "" || req.body.password == "") {
    res.status(401).json({
      "Status": "401",
      "message": "All fields are required"
    });
  } else {
    var user = new User({
      email: email,
      password: pwd,
      created_date: c_date,
      modified_date: m_date,
      is_deleted: "0",
      token: token_en,
      social_id: "",
      user_name: user_name,
      user_type: "U",
      profile_image: "",
      profile_status:"0",
      device_id: "",
      device_type: ""
    });
    user
      .save()
      .then(result => {
        res.status(200).json({
          status: "200",
          message: "Registered Successfully",
          user_id: user._id,
          token: user.token,
          profile_status: user.profile_status
        });
      })
      .catch(err => {
        res.status(400).json({
          "Status": "400",
          "message": "Email already Registered"
        });
      });
  }
});

//Social Login API

/*router.post('/sociallogin', upload.single('image'), function (req, res, next) {
  User.findOne({ 'social_id': req.body.social_id }, function (err, data) {
    console.log(data)
    if (data) {
      var social_id = req.body.social_id;
      var user_name = req.body.user_name;
      var email = req.body.email;
      var type = req.body.type;
      var random_num = Math.floor(Math.random() * 1000000) + 1;
      var de_token = random_num.toString();
      var mykey = crypto.createCipher('aes-128-cbc', 'token');
      var token_en = mykey.update(de_token, 'utf8', 'hex') + mykey.final('hex');
      var user = new User({
        social_id: social_id,
        type: type,
        user_name: user_name,
        token: token_en,
        email: email
      });
      user.save().then(result => {
        res.status(200).json({
          status: "1",
          message: "Registration Successfully",
          user_id: user._id,
          token: user.token,
          "register_or_login": 'R'
        });
      }).catch();
    }
    else {
      res.json({
        "status": "1",
        "status_message": "Logged in Successfully",
        "user_id": data._id,
        "token": data.token,
        "register_or_login": 'L'
      });
    }
  });
});*/

//Login API

router.post('/login', upload.single('image'), function (req, res, next) {
  var email = req.body.email.toLowerCase();
  var password = req.body.password;
  var pwd = md5(password);
  User.findOne({ 'email': email }, function (err, checkemail) {
    if (checkemail) {
      User.findOne({ 'email': email, 'status': 'Unban' }, function (err, userdata) {
        if (userdata) {
          if (userdata.email == email && userdata.password == pwd) {
            res.json({
              "status": "1",
              "status_message": "Logged in Successfully",
              "user_id": userdata._id,
              "token": userdata.token,
              "profile_status": userdata.profile_status,
            }); return;
          } else {
            res.json({
              "status": "0",
              "status_message": "Wrong Credentials.",
            }); return;
          }
        } else {
          res.json({
            "status": "0",
            "status_message": "You are Banned.",
          }); return;
        }
      })
    } else {
      res.json({
        "status": "0",
        "status_message": "Email is not Registered.",
      }); return;
    }
  })
});

//Forgot password API

router.post('/forgot', function(req,res,next){
    var email = req.body.email;
    var randomstring = Genpassword.randomPassword({
      characters: [
      { characters: Genpassword.upper, exactly: 1 },
      { characters: Genpassword.symbols, exactly: 1 },
      { characters: Genpassword.digits, exactly: 2 },
      Genpassword.lower]
    });
    User.findOne({ 'email': req.body.email }, function (err, data) {
      if (data) {
        console.log(data)
        var myquery = { _id: data._id };
        var en_pass = md5(randomstring);
        var newvalues = { $set: { password: en_pass } };

        User.updateOne(myquery, newvalues, function (err, result) {
          var transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
              user: 'php.mspl@gmail.com',
              pass: 'S@needp6),/'
            }
          });
          var mailOptions = {
            from: 'php.mspl@gmail.com',
            to: email,
            subject: 'Forgot Password',
            text: 'Your new password is ' + randomstring
          };

          transporter.sendMail(mailOptions, function (error, info) {
          console.log(error)
          if (error) {
            res.json({
              "status": "0",
              "status_message": "Something went wrong"
            });
          } else {
            res.json({"status": "1","status_message": "New Password has been sent to your email address"
          });
          }
        });
      });
      }else{
        res.json({"status": "0","status_message": "Email is not Registered",}); return;
      }
    });
});

router.post('/forgot1', upload.single('image'), function (req, res, next) {
  var email = req.body.email;
  var randomstring = Genpassword.randomPassword({
    characters: [
      { characters: Genpassword.upper, exactly: 1 },
      { characters: Genpassword.symbols, exactly: 1 },
      { characters: Genpassword.digits, exactly: 2 },
      Genpassword.lower]
  });
  User.findOne({ 'email': email }, function (err, data) {
    if (data) {
      var myquery = { _id: data._id };
      var en_pass = md5(randomstring);
      var newvalues = { $set: { password: en_pass } };
      /*saving password to database*/
      User.updateOne(myquery, newvalues, function (err, result) {
        var transporter = nodemailer.createTransport({
          service: 'gmail',
          auth: {
            user: 'php.mspl@gmail.com',
            pass: 'S@needp6),/'
          }
        });
        var mailOptions = {
          from: 'sankhyan.amit@gmail.com',
          to: email,
          subject: 'Forgot Password',
          text: 'Your new password is ' + randomstring
        };
        transporter.sendMail(mailOptions, function (error, info) {
          console.log(error)
          if (error) {
            res.json({
              "status": "0",
              "status_message": "Please Enter Your Registered Email"
            });
          } else {
            res.json({
              "status": "1",
              "status_message": "New Password has been sent to your email address"
            });
          }
        });
      });
    }
    else {
      res.json({
        "status": "0",
        "status_message": "Email is not Registered",
      }); return;
    }
  });
});

//Edit / Create pofile API

router.post('/edit_profile', upload_profile_image.single('image'), (req, res, next) => {
  if (!req.headers.token) {
    res.json({
      "status": "0",
      "status_message": "Fields are required",
    }); return;
  }
  else {
    User.findOne({ 'token': req.headers.token }, function (err, data) {
      try {
        if (err) {
          res.json({
            "status": "0",
            "status_message": "Please enter correct data"
          }); return;
        } else {
          if (data) {
            User.findOne({ '_id': data._id }, function (err, data2) {
              if (data2) {
                // var user_name = !req.body.user_name ? data2.user_name : req.body.user_name;
                var user_name = !req.body.user_name ? data2.user_name : req.body.user_name;
                var phone_no = !req.body.phone_no ? data2.phone_no : req.body.phone_no;
                var email = !req.body.email ? data2.email : req.body.email;
                var city = !req.body.city ? data2.city : req.body.city;
                var state = !req.body.state ? data2.state : req.body.state;
                var exp_sponser_name = !req.body.exp_sponser_name ? data2.exp_sponser_name : req.body.exp_sponser_name;
                var licenced_years = !req.body.licenced_years ? data2.licenced_years : req.body.licenced_years;
                if (!req.file) {
                  image = "";
                }
                else {
                  image = req.file.originalname;
                }
                var myquery = { _id: data._id };
                var newvalues = {
                  $set: {
                    user_name: user_name,
                    phone_no: phone_no,
                    email: email,
                    city: city,
                    profile_image: image,
                    state: state,
                    exp_sponser_name: exp_sponser_name,
                    licenced_years: licenced_years,
                    profile_status:"1"
                  }
                };
                User.updateMany(myquery, newvalues, function (err, result) {
                  if (result) {
                    console.log(result)
                    User.findOne({ '_id': data._id }, function (err, data3) {
                      if (data3) {
                        res.json({
                          "status": "1",
                          "status_message": "Profile Updated Successfully"
                        }); return;
                      }
                    });
                  }
                });
              }
            });
          } else {
            res.json({
              "status": "0",
              "status_message": "User not Found",
            }); return;
          }
        }
      } catch (e) {
        res.json({ "status": "0", "status_message": "fghfh", 'error': e }); return;
      }
    });
  }
});


//----------------get profile -----------------//

router.post('/get_profile', upload.single('image'), (req, res, next) => {
  if (!req.headers.token) {
    res.json({
      "status": "0",
      "status_message": "Fields are required",
    }); return;
  }
  else {
    User.findOne({ 'token': req.headers.token }, function (err, data) {
      try {
        if (err) {
          res.json({
            "status": "0",
            "status_message": "Fields are required"
          }); return;
        } else {
          if (data) {
            if (data.token != req.headers.token) {
              res.json({
                "status": "0",
                "status_message": "User does not Exist.",
              }); return;
            } else {
              User.find({ '_id': data._id }, function (err, data) {
                if (data) {
                  data.forEach(function (element) {
                    if (element.profile_image == "") {
                      var profile_image = baseurl;
                    } else {
                      var profile_image = profile_images + element.profile_image;
                    }
                    // if (element.activitypoints < element.salepoints) {
                    //   badges_points = element.salepoints
                    // }else {
                    //   badges_points = element.activitypoints
                    // }
                    res.json({
                      "status": "1",
                      "badges_points": element.badges_points,
                      "activitypoints": element.activitypoints,
                      "salepoints": element.salepoints,
                      "user_id": element._id,
                      "user_name": element.user_name.capitalize(),
                      "email": element.email,
                      "phone_no": element.phone_no,
                      "city": element.city.capitalize(),
                      "state": element.state.capitalize(),
                      "exp_sponser_name": element.exp_sponser_name.capitalize(),
                      "licenced_years": element.licenced_years,
                      "ban_status": element.status,
                      "profile_image": profile_image
                    }); return;
                  });
                }
              });
            }
          } else {
            res.json({
              "status": "0",
              "status_message": "User not Found",
            }); return;
          }
        }
      } catch (e) {
        res.json({ "status": "0", "status_message": "Something went wrong", 'error': e }); return;
      }
    });
  }
});

//Change Password
router.post('/change_password', upload.single('profile_image'), (req, res, next) => {
  if (!req.headers.token) {
    res.json({
      "status": "0",
      "status_message": "Fields are required",
    }); return;
  }
  else {
    User.findOne({ 'token': req.headers.token }, function (err, data) {
      try {
        if (err) {
          res.json({
            "status": "0",
            "status_message": "User not Found"
          }); return;
        } else {
          if (data) {
            if (data.token != req.headers.token) {
              res.json({
                "status": "0",
                "status_message": "User does not Exist.",
              }); return;
            } else {
              // var dcrypass = reverseMd5(data.password);
              // console.log(dcrypass);
              var pwd_old = req.body.old_password;
              var pwd_old1 = md5(pwd_old);
              if (pwd_old1 == data.password) {
                console.log(data.password);
                var new_password = req.body.new_password;
                var confirm_password = req.body.confirm_password;
                if (new_password == confirm_password) {
                  var new_password1 = md5(new_password);
                  var myquery = { _id: data._id };
                  var newvalues = { $set: { password: new_password1 } };
                  User.updateOne(myquery, newvalues, function (err, result) {
                    res.json({
                      "status": "1",
                      "status_message": "Password updated",
                    }); return;
                  });
                } else {
                  res.json({
                    "status": "0",
                    "status_message": "New password or confirm Password are not matched",
                  }); return;
                }
              } else {
                res.json({
                  "status": "0",
                  "status_message": "old password not matched."
                  // "data":data
                }); return;
              }
            }
          } else {
            res.json({
              "status": "0",
              "status_message": "User not Found",
            }); return;
          }
        }
      } catch (e) {
        res.json({ "status": "0", "status_message": "fghfh", 'error': e }); return;
      }
    });
  }
});

//Add Activity
router.post('/add_activity', upload.single('number_of_images'), (req, res, next) => {
  if (!req.headers.token) {
    res.json({
      "status": "0",
      "status_message": "Fields are required",
    }); return;
  }
  else {
    User.findOne({ 'token': req.headers.token }, function (err, data) {
      try {
        if (err) {
          res.json({
            "status": "0",
            "status_message": "Fields are required"
          }); return;
        } else {
          if (data) {
            if (data.token != req.headers.token) {
              res.json({
                "status": "0",
                "status_message": "User does not Exist.",
              }); return;
            } else {
              var type = req.body.type;
              var activity_name = req.body.activity_name;
              var activity_description = req.body.activity_description;
              var activity_points = req.body.activity_points;
              var c_date = req.body.date;
              var m_date = req.body.date;
              var activity = new Activity({
                activity_name: activity_name,
                activity_description: activity_description,
                activity_points: activity_points,
                type: type,
                created_date: c_date,
                modified_date: m_date,
                is_deleted: "0"
              });
              activity
                .save()
                .then(result => {
                  res.status(201).json({
                    status: "1",
                    message: "Activity added successfully",
                  });
                })
                .catch(err => {
                  res.status(500).json({
                    "Status": "0",
                    "message": "Error"
                  });
                });
            }
          } else {
            res.json({
              "status": "0",
              "status_message": "User not Found",
            }); return;
          }
        }
      } catch (e) {
        res.json({ "status": "0", "status_message": "fghfh", 'error': e }); return;
      }
    });
  }
});

//Get Activities

router.post('/get_activities', upload.single('number_of_images'), (req, res, next) => {
  if (!req.headers.token) {
    res.json({
      "status": "0",
      "status_message": "Fields are required",
    }); return;
  }
  else {
    User.findOne({ 'token': req.headers.token }, function (err, data) {
      try {
        if (err) {
          res.json({
            "status": "0",
            "status_message": "Fields are required"
          }); return;
        } else {
          if (data) {
            if (data.token != req.headers.token) {
              res.json({
                "status": "0",
                "status_message": "User does not Exist.",
              }); return;
            } else {
              var userData = data;
              if (req.body.type == 'activity') {
                userData = data;
                Activity.find({ is_deleted: '0', type: 'activity' }, function (err, data) {
                  /*Activity Points*/
                  ActivityPoint.find({ user_id: userData._id, type: 'activity' }, function (err, dataa) {
                    var arr = dataa;
                    var total = 0;
                    arr.forEach(item => {
                      total += item.activity_point;
                    });
                    /*Activity Points*/
                    var record = [];
                    data.forEach(function (data3) {
                      record.push({
                        "_id": data3._id,
                        "activity_name": data3.activity_name.capitalize(),
                        "activity_description": data3.activity_description.capitalize(),
                        "activity_points": data3.activity_points,
                        "type": data3.type,
                        "created_date": data3.created_date
                      });
                    });
                    res.json({
                      "status": "1",
                      "status_message": "Activities Found",
                      "activity_points": total,
                      "activity": record,
                    });
                  });
                });
              } else if (req.body.type == 'sale') {
                Activity.find({ is_deleted: '0', type: 'sale' }, function (err, data) {
                  /*Sales Points*/
                  ActivityPoint.find({ user_id: userData._id, type: 'sale' }, function (err, dataa) {
                    var arr = dataa;
                    var total = 0;
                    arr.forEach(item => {
                      total += item.activity_point;
                    });
                    /*Sales Points*/
                    var record = [];
                    data.forEach(function (data3) {
                      record.push({
                        "_id": data3._id,
                        "activity_name": data3.activity_name.capitalize(),
                        "activity_description": data3.activity_description.capitalize(),
                        "activity_points": data3.activity_points,
                        "type": data3.type,
                        "created_date": data3.created_date
                      });
                    });
                    res.json({
                      "status": "1",
                      "status_message": "Sales Found",
                      "sales_points": total,
                      "sales": record,
                    });
                  });
                });
              } else if (req.body.type == '') {
                Activity.find({ is_deleted: '0' }, function (err, data) {
                  var record = [];
                  data.forEach(function (data3) {
                    record.push({
                      "_id": data3._id,
                      "activity_name": data3.activity_name.capitalize(),
                      "activity_description": data3.activity_description,
                      "activity_points": data3.activity_points,
                      "type": data3.type,
                      "created_date": data3.created_date
                    });
                  });
                  res.json({
                    "status": "1",
                    "status_message": "Activity and Sales Found",
                    "All_Activities": record,
                  });
                });
              }
            }
          } else {
            res.json({
              "status": "0",
              "status_message": "User not Found",
            }); return;
          }
        }
      } catch (e) {
        res.json({ "status": "0", "status_message": "fghfh", 'error': e }); return;
      }
    });
  }
});

/*await functions API -> create_activity new*/

let getPoints = (activityid) => {
  return new Promise((resolve, reject) => {
    Activity.findOne({ '_id': activityid, is_deleted: '0' }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    });

  })
}

let actPoints = (user_id, points) => {
  return new Promise((resolve, reject) => {
    User.findOneAndUpdate({ _id: user_id }, { $inc: { activitypoints: points } }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    });
  })
}

let salePoints = (user_id, points) => {
  return new Promise((resolve, reject) => {
    User.findOneAndUpdate({ _id: user_id }, { $inc: { salepoints: points } }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    });
  })
}

let allPoints = (user_id) => {
  return new Promise((resolve, reject) => {
    User.findOne({ _id: user_id }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    });
  })
}

let updateBadge = (user_id, currentbadge) => {
  return new Promise((resolve, reject) => {
    User.findOneAndUpdate(
      { "_id": user_id },
      { $set: { "badges_points": currentbadge } }
    ).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    });
  })
}

let getBadgePoint = (id) => {
  return new Promise((resolve, reject) => {
    User.findOne({ '_id': id }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    });

  })
}

router.post('/create_activity', upload.single('number_of_images'), async (req, res, next) => {
  if (!req.headers.token || !req.body.activity_id || !req.body.activity_date) {
    res.json({
      "status": "0",
      "status_message": "Fields are required",
    }); return;
  }
  /* calling a function await created on top */
  let authUser = await userAuthentication(req.headers.token)
  if (authUser) {
    const createdactivity = new CreatedActivity({
      user_id: authUser._id,
      activity_id: req.body.activity_id,
      activity_date: req.body.activity_date,
      created_date: req.body.date,
      modified_date: req.body.date,
      is_deleted: "0"
    })
    let create_activity = await createdactivity.save();
    /*call the functions*/
    //getting single activity point from body activity_id
    const points = await getPoints(req.body.activity_id)
    let apoints = points.activity_points;
    let type = points.type;
    if (type == 'activity') {
      /*call the functions*/
      const act_points = await actPoints(authUser._id, apoints)
    }
    else {
      /*call the functions*/
      const sale_points = await salePoints(authUser._id, apoints)
    }
    const activitypoint = new ActivityPoint({
      user_id: authUser._id,
      activity_id: req.body.activity_id,
      activity_point: apoints,
      type: type,
      created_date: req.body.date,
      modified_date: req.body.date,
      is_deleted: "0"
    });
    /*call the functions*/
    /*activity & sale point of user*/
    const all_points = await allPoints(authUser._id)
    let activitySum = all_points.activitypoints
    let saleSum = all_points.salepoints
    /*current badges points*/
    if (activitySum < saleSum) {
      var currentbadge = saleSum
    } else {
      var currentbadge = activitySum
    }
    /*current badges points*/

    /*last badge point*/
    const highestbadge = await getBadgePoint(authUser._id)
    const lastBadge = highestbadge.badges_points
    /*last badge point*/
    console.log(currentbadge);
    console.log(lastBadge);
    if (currentbadge <= lastBadge) {
      const update_badge = await updateBadge(authUser._id, lastBadge)
    } else {
      /*update highest point for getting badge*/
      const update_badge = await updateBadge(authUser._id, currentbadge)
      /*update highest point for getting badge*/
    }
    let create_activityP = await activitypoint.save().then(result => {
      res.status(200).json({
        status: "1",
        message: "Activity Created successfully",
      });
    })
      .catch(err => {
        res.status(400).json({
          "Status": "0",
          "message": "Error"
        });
      });
  } else {
    console.log('toekn mismatch')
    res.status(400).json({
      "status": "0",
      "message": "Invalid Token"
    });
  }
});

/*await functions API -> view agent*/

let getUser = (id) => {
  return new Promise((resolve, reject) => {
    User.findOne({ '_id': id }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    });

  })
}

let userAuthentication = (token) => {
  return new Promise((resolve, reject) => {
    User.findOne({ 'token': token }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    });

  })
}


let getUserList = (authUser) => {
  console.log(authUser._id);
  return new Promise((resolve, reject) => {
    User.find({ _id: {$ne: authUser._id},user_type: { $not: { $lte: 'A' } } }).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    });

  })
}

let getSalePoint = (id) => {
  return new Promise((resolve, reject) => {
    ActivityPoint.find({ user_id: id, type: 'sale' }).then(response => {
      return resolve(response);

    }).catch(error => {
      return reject(error);
    });

  })
}

let getActivityPoint = (id) => {
  return new Promise((resolve, reject) => {
    ActivityPoint.find({ user_id: id, type: 'activity' }).then(response => {
      return resolve(response);

    }).catch(error => {
      return reject(error);
    });

  })
}


//View Agents
router.post('/view_agents', upload.single('number_of_images'), async (req, res, next) => {
  if (!req.headers.token) {
    res.json({
      "status": "0",
      "status_message": "Fields are required",
    }); return;
  }
  else {
    /* calling a function await created on top */
    let authUser = await userAuthentication(req.headers.token)
    if (authUser) {
      if (req.body.user_id == '') {
        let allusers = await getUserList(authUser)
        if (allusers) {
          // res.json(allusers)
          var record = [];
          allusers.forEach(function (data3) {
            if (data3.profile_image == "") {
              var profile_image = baseurl;
            } else {
              var profile_image = profile_images + data3.profile_image;
            }
            record.push({
              "_id": data3._id,
              "user_name": data3.user_name.capitalize(),
              "email": data3.email,
              "phone_no": data3.phone_no,
              "city": data3.city.capitalize(),
              "state": data3.state.capitalize(),
              "exp_sponser_name": data3.exp_sponser_name.capitalize(),
              "licenced_years": data3.licenced_years,
              "profile_image": profile_image
            });
          });
          res.json({
            "status": "1",
            "status_message": "Agents Found",
            "All_agents": record,
          });
          console.log("all users")
        } else {
          console.log("no users")
          res.json({
            "status": "1",
            "status_message": "No Agents Found",
          });
        }
      } else {
        /*single user details*/
        const user = await getUser(req.body.user_id)
        /* sales details pounts */
        let salesPoints = await getSalePoint(user._id)
        sales = [];
        let sales_Point = 0;
        salesPoints.forEach(item => {
          sales_Point += item.activity_point;
        });
        sales.push({ sales_Point: sales_Point });
        /* activity points details */
        let activityPoints = await getActivityPoint(user._id)
        activity = [];
        let activity_Point = 0;
        activityPoints.forEach(item => {
          activity_Point += item.activity_point;
        });
        activity.push({ activity_Point: activity_Point });
        if (user.profile_image == "") {
          var profile_image = baseurl;
        } else {
          var profile_image = profile_images + user.profile_image;
        }
        res.json({
          "status": "1",
          "status_message": "Agent Details Found",
          "_id": user._id,
          "user_name": user.user_name.capitalize(),
          "email": user.email,
          "phone_no": user.phone_no,
          "city": user.city.capitalize(),
          "state": user.state.capitalize(),
          "exp_sponser_name": user.exp_sponser_name.capitalize(),
          "licenced_years": user.licenced_years,
          "profile_image": profile_image,
          "ban_status": user.status,
          "sales_points": sales,
          "activity_points": activity
        }); return;
      }
    } else {
      console.log('toekn mismatch')
      res.status(400).json({
        "status": "0",
        "message": "Invalid Token"
      });
    }
  }
});

//Search Agents

router.post('/search_agent', upload.single('image'), (req, res, next) => {
  if (!req.headers.token || !req.body.search) {
    res.json({
      "status": "0",
      "status_message": "Fields are required",
    }); return;
  }
  else {
    User.findOne({ 'token': req.headers.token }, function (err, data) {
      try {
        if (err) {
          res.json({
            "status": "0",
            "status_message": "Fields are required"
          }); return;
        } else {
          if (data) {
            if (data.token != req.headers.token) {
              res.json({
                "status": "0",
                "status_message": "User does not Exist.",
              }); return;
            } else {
              var search = req.body.search;
              User.find({
                $or: [
                  { 'user_name': new RegExp(search, 'i') },
                  { 'email': new RegExp(search, 'i') },
                  { 'phone_no': new RegExp(search, 'i') }
                ],
                $and: [
                  { _id: { $ne: data._id } }
                ]
              }, function (err, data) {
                if (data != '') {
                  var record = [];
                  data.forEach(function (data3) {
                    if (data3.profile_image == "") {
                      var profile_image = baseurl;
                    } else {
                      var profile_image = profile_images + data3.profile_image;
                    }
                    record.push({
                      "_id": data3._id,
                      "user_name": data3.user_name.capitalize(),
                      "email": data3.email,
                      "phone_no": data3.phone_no,
                      "city": data3.city.capitalize(),
                      "state": data3.state.capitalize(),
                      "exp_sponser_name": data3.exp_sponser_name.capitalize(),
                      "licenced_years": data3.licenced_years,
                      "profile_image": profile_image
                    });
                  });
                  res.json({
                    "status": "1",
                    "status_message": "Agents Found",
                    "listing": record,
                  });
                } else {
                  res.json({
                    "status": "1",
                    "status_message": "No Agent Found",
                  });
                }
              });
            }
          } else {
            res.json({
              "status": "0",
              "status_message": "Invalid User",
            }); return;
          }
        }
      } catch (e) {
        res.json({ "status": "0", "status_message": "Something went wrong", 'error': e }); return;
      }
    });
  }
});

router.post('/activityAgents', upload.single('number_of_images'), (req, res, next) => {
  if (!req.headers.token) {
    res.json({
      "status": "0",
      "status_message": "Fields are required",
    }); return;
  }
  else {
    if (req.body.id == "") {
      activity = 'activity'
      ActivityPoint.aggregate([
        { "$match": { "type": activity } },
        { $group: { _id: { user_id: "$user_id" }, total: { $sum: "$activity_point" } } },
        {
          $lookup: {
            from: "users",
            localField: "_id.user_id",
            foreignField: "_id",
            as: "userData"
          }
        },
        {
          $unwind: "$userData"
        },
        {
          "$sort": { "total": -1 }
        },
      ], function (err, data) {
        if (data != '') {
          var topUsers = []
          var rank = 1
          data.forEach(function (element) {
            let user = {
              userId: element.userData._id,
              userName: element.userData.user_name.capitalize(),
              points: element.total,
              city: element.userData.city.capitalize(),
              state: element.userData.state.capitalize(),
              rank: rank
            }
            rank++
            topUsers.push(user)
          });
          res.json({
            "status": "1",
            "status_message": "Overall Agents Found",
            "listing": topUsers,
          });
        } else {
          res.json({
            "status": "1",
            "status_message": "No Agents Found",
          });
        }
      })
    } else {
      activityId = req.body.id
      ActivityPoint.aggregate([
        { "$match": { "activity_id": ObjectId(activityId) } },
        { $group: { _id: { user_id: "$user_id" }, total: { $sum: "$activity_point" } } },
        {
          $lookup: {
            from: "users",
            localField: "_id.user_id",
            foreignField: "_id",
            as: "userData"
          }
        },
        {
          $unwind: "$userData"
        },
        {
          "$sort": { "total": -1 }
        },
      ], function (err, data) {
        if (data != '') {
          var topUsers = []
          var rank = 1
          data.forEach(function (element) {
            let user = {
              userId: element.userData._id,
              userName: element.userData.user_name.capitalize(),
              points: element.total,
              city: element.userData.city.capitalize(),
              state: element.userData.state.capitalize(),
              rank: rank
            }
            rank++
            topUsers.push(user)
          });
          res.json({
            "status": "1",
            "status_message": "Agents Found",
            "listing": topUsers,
          });
        } else {
          res.json({
            "status": "1",
            "status_message": "No Agents Found",
          });
        }
      })
    }
  }
});

router.post('/salesAgents', upload.single('number_of_images'), (req, res, next) => {
  if (!req.headers.token) {
    res.json({
      "status": "0",
      "status_message": "Fields are required",
    }); return;
  }
  else {
    if (req.body.id == "") {
      sale = 'sale'
      ActivityPoint.aggregate([
        { "$match": { "type": sale } },
        { $group: { _id: { user_id: "$user_id" }, total: { $sum: "$activity_point" } } },
        {
          $lookup: {
            from: "users",
            localField: "_id.user_id",
            foreignField: "_id",
            as: "userData"
          }
        },
        {
          $unwind: "$userData"
        },
        {
          "$sort": { "total": -1 }
        },
      ], function (err, data) {
        if (data != '') {
          var topUsers = []
          var rank = 1
          data.forEach(function (element) {

            let user = {
              userId: element.userData._id,
              userName: element.userData.user_name.capitalize(),
              points: element.total,
              city: element.userData.city.capitalize(),
              state: element.userData.state.capitalize(),
              rank: rank
            }
            rank++
            topUsers.push(user)
          });

          res.json({
            "status": "1",
            "status_message": "Overall Agents Found",
            "listing": topUsers,
          });
        } else {
          res.json({
            "status": "1",
            "status_message": "No Agents Found",
          });
        }
      })
    } else {
      activityId = req.body.id
      console.log(activityId);
      ActivityPoint.aggregate([
        // {
        //   "$match" : {_id : {activity_id: "$activity_id"}}
        // },
        { "$match": { "activity_id": ObjectId(activityId) } },
        { $group: { _id: { user_id: "$user_id" }, total: { $sum: "$activity_point" } } },
        {
          $lookup: {
            from: "users",
            localField: "_id.user_id",
            foreignField: "_id",
            as: "userData"
          }
        },
        {
          $unwind: "$userData"
        },
        {
          "$sort": { "total": -1 }
        },
      ], function (err, data) {
        console.log(data);
        if (data != '') {
          var topUsers = []
          var rank = 1
          data.forEach(function (element) {

            let user = {
              userId: element.userData._id,
              userName: element.userData.user_name.capitalize(),
              points: element.total,
              city: element.userData.city.capitalize(),
              state: element.userData.state.capitalize(),
              rank: rank
            }
            rank++
            topUsers.push(user)
          });
          res.json({
            "status": "1",
            "status_message": "Agents Found",
            "listing": topUsers,
          });
        } else {
          res.json({
            "status": "1",
            "status_message": "No Agents Found",
          });
        }
      })
    }
  }
});

let actHistory = (userId, activity) => {
  return new Promise((resolve, reject) => {
    ActivityPoint.aggregate([
      { "$match": { "user_id": ObjectId(userId), "type": activity } },
      { $group: { _id: { activity_id: "$activity_id" }, totalPoints: { $sum: "$activity_point" } } },
      {
        $lookup: {
          from: "activities",
          localField: "_id.activity_id",
          foreignField: "_id",
          as: "activityData"
        }
      },
      {
        $unwind: "$activityData"
      },
    ]).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    });

  })
}

let saleHistory = (userId, sale) => {
  return new Promise((resolve, reject) => {
    ActivityPoint.aggregate([
      { "$match": { "user_id": ObjectId(userId), "type": sale } },
      { $group: { _id: { activity_id: "$activity_id" }, totalPoints: { $sum: "$activity_point" } } },
      {
        $lookup: {
          from: "activities",
          localField: "_id.activity_id",
          foreignField: "_id",
          as: "activityData"
        }
      },
      {
        $unwind: "$activityData"
      },
    ]).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    });
  })
}

let actRank = (activityId, userId) => {
  return new Promise((resolve, reject) => {
    ActivityPoint.aggregate([
      { "$match": { "activity_id": ObjectId(activityId) } },
      { $group: { _id: { user_id: "$user_id" }, total: { $sum: "$activity_point" } } },
      {
        "$sort": { "total": -1 }
      },
    ]).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    });
  })
}

let saleRank = (saleId, userId) => {
  return new Promise((resolve, reject) => {
    ActivityPoint.aggregate([
      { "$match": { "sale_id": ObjectId(saleId) } },
      { $group: { _id: { user_id: "$user_id" }, total: { $sum: "$activity_point" } } },
      {
        "$sort": { "total": -1 }
      },
    ]).then(response => {
      return resolve(response);
    }).catch(error => {
      return reject(error);
    });
  })
}
/*new history with rank*/
router.post('/history', upload.single('number_of_images'), async (req, res, next) => {
  if (!req.headers.token || !req.body.type || !req.body.user_id) {
    res.json({
      "status": "0",
      "status_message": "Fields are required",
    }); return;
  }
  else {
    let authUser = await userAuthentication(req.headers.token)
    if (authUser) {
      if (req.body.type == 'activity') {
        let userId = req.body.user_id
        let activity = 'activity'
        let act_history = await actHistory(userId, activity)
        if (act_history == '') {
          res.json({
            "status": "1",
            "status_message": "No History Yet",
          });
        }
        let  actData = []
        act_history.forEach(async actDetails => {
          let activityId = actDetails._id.activity_id
          var actHistory = await actRank(activityId)
          var rankk = 1
          let topU = []
          actHistory.forEach(element => {
            let userrank = {
              userId: element._id.user_id,
              points: element.total,
              rank: rankk
            }
            rankk++
            topU.push(userrank)
          });
          const filterArray = req.body.user_id
          var result = topU.filter(({ userId }) => filterArray.includes(userId));
            actData.push({
            "activity_id":actDetails._id.activity_id,
            "points": actDetails.totalPoints,
            "activity_name":actDetails.activityData.activity_name.capitalize(),
            "rank":result[0].rank
          });
          if (actData.length === act_history.length) {
            res.json({
              "status": "1",
              "status_message": "activity history Found",
              "activity_data": actData,
            });
          }
        });
      } else if (req.body.type == 'sale') {
        let userId = req.body.user_id
        let sale = 'sale'
        let act_history = await saleHistory(userId, sale)
        if (act_history == '') {
          res.json({
            "status": "1",
            "status_message": "No History Yet",
          });
        }
        let  actData = []
        act_history.forEach(async actDetails => {
          let activityId = actDetails._id.activity_id
          var actHistory = await actRank(activityId)
          var rankk = 1
          let topU = []
          actHistory.forEach(element => {
            let userrank = {
              userId: element._id.user_id,
              points: element.total,
              rank: rankk
            }
            rankk++
            topU.push(userrank)
          });
          const filterArray = req.body.user_id
          var result = topU.filter(({ userId }) => filterArray.includes(userId));
            actData.push({
            "sales_id":actDetails._id.activity_id,
            "points": actDetails.totalPoints,
            "sales_name":actDetails.activityData.activity_name.capitalize(),
            "rank":result[0].rank
          });
          if (actData.length === act_history.length) {
            res.json({
              "status": "1",
              "status_message": "sales history Found",
              "activity_data": actData,
            });
          }
        });
      }
    } else {
      console.log('token mismatch')
      res.status(400).json({
        "status": "0",
        "message": "Invalid Token"
      });
    }
  }
})

router.post('/getNearbyPlaces', upload_profile_image.single('image'), (req, res, next) => {
  var lat = req.body.lat
  var lng = req.body.lng
  
})

module.exports = router;
