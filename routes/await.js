  /*await functions API -> view agent*/

  router.post('/history', upload.single('number_of_images'), (req, res, next) => {
    if (!req.headers.token || !req.body.type || !req.body.user_id) {
      res.json({
        "status": "0",
        "status_message": "Fields are required",
      }); return;
    }
    else {
      User.findOne({ 'token': req.headers.token }, function (err, userData) {
        if (userData != '') {
          if (req.body.type == 'activity') {
            userId = req.body.user_id
            activity = 'activity'
            ActivityPoint.aggregate([
              { "$match": { "user_id": ObjectId(userId), "type": activity } },
              { $group: { _id: { activity_id: "$activity_id" }, totalPoints: { $sum: "$activity_point" } } },
              {
                $lookup: {
                  from: "activities",
                  localField: "_id.activity_id",
                  foreignField: "_id",
                  as: "activityData"
                }
              },
              {
                $unwind: "$activityData"
              },
            ], function (err, data) {
              if (data != '') {
                var topUsers = []
                var rank = 1
                data.forEach(function (element) {
                  /*rank of user*/
                  let activityId = element._id.activity_id
                  ActivityPoint.aggregate([
                    { "$match": { "activity_id": ObjectId(activityId) } },
                    { $group: { _id: { user_id: "$user_id" }, total: { $sum: "$activity_point" } } },
                    {
                      "$sort": { "total": -1 }
                    },
                  ]).exec(function(error, fetchAllTopUsers){
                      console.log('##################');
                      let topU = []
                      var rank = 1
                      fetchAllTopUsers.forEach(function (element) {
                        let user = {
                          userId: element._id.user_id,
                          points: element.total,
                          rank: rank
                        }
                        rank++
                        topU.push(user)
                      });
                      const array = topU
                      const filterArray = req.body.user_id
                      const result = array.filter(({ userId }) => filterArray.includes(userId));
                      console.log(result)
                  });
                  /*rank of user*/
                  let user = {
                    activity_id: element._id.activity_id,
                    points: element.totalPoints,
                    activity_name: element.activityData.activity_name.capitalize(),
                    rank: rank
                  }
                  rank++
                  topUsers.push(user)
                });
                res.json({
                  "status": "1",
                  "status_message": "Activities History Found",
                  "listing": topUsers,
                });
              } else {
                res.json({
                  "status": "1",
                  "status_message": "No Data Found",
                });
              }
            })
          } else if (req.body.type == 'sale') {
            userId = req.body.user_id
            sale = req.body.type
            ActivityPoint.aggregate([
              { "$match": { "user_id": ObjectId(userId), "type": sale } },
              { $group: { _id: { activity_id: "$activity_id" }, totalPoints: { $sum: "$activity_point" } } },
              {
                $lookup: {
                  from: "activities",
                  localField: "_id.activity_id",
                  foreignField: "_id",
                  as: "activityData"
                }
              },
              {
                $unwind: "$activityData"
              },
              // {
              //   "$sort": { "total": -1 }
              // },
            ], function (err, data) {
              if (data != '') {
                var topUsers = []
                var rank = 1
                data.forEach(function (element) {
                  let user = {
                    sales_id: element._id.activity_id,
                    points: element.totalPoints,
                    sales_name: element.activityData.activity_name.capitalize(),
                    rank: rank
                  }
                  rank++
                  topUsers.push(user)
                });
                res.json({
                  "status": "1",
                  "status_message": "Sales History Found",
                  "listing": topUsers,
                });
              } else {
                res.json({
                  "status": "1",
                  "status_message": "No Data Found",
                });
              }
            })
          }
        } else {
          res.json({
            "status": "0",
            "status_message": "Invalid Token",
          });
        }
      })
    }
  });


  let getUser = (id) => {
    return new Promise((resolve, reject) => {
      User.findOne({ '_id': id }).then(response => {
        return resolve(response);
      }).catch(error => {
        return reject(error);
      });

    })
  }

  let userAuthentication = (token) => {
    return new Promise((resolve, reject) => {
      User.findOne({ 'token': token }).then(response => {
        return resolve(response);
      }).catch(error => {
        return reject(error);
      });

    })
  }


  let getUserList = () => {
    return new Promise((resolve, reject) => {
      User.find( { user_type: { $not: { $lte: 'A' } } } ).then(response => {
        return resolve(response);
      }).catch(error => {
        return reject(error);
      });

    })
  }

  let getSalePoint = (id) => {
    return new Promise((resolve, reject) => {
      ActivityPoint.find({ user_id: id, type: 'sale' }).then(response => {
        return resolve(response);

      }).catch(error => {
        return reject(error);
      });

    })
  }

  let getActivityPoint = (id) => {
    return new Promise((resolve, reject) => {
      ActivityPoint.find({ user_id: id, type: 'activity' }).then(response => {
        return resolve(response);

      }).catch(error => {
        return reject(error);
      });
    })
  }




  //View Agents
  router.post('/view_agents', upload.single('number_of_images'), async (req, res, next) => {
    if (!req.headers.token) {
      res.json({
        "status": "0",
        "status_message": "Fields are required",
      }); return;
    }
    else {
      /* calling a function await created on top */
      let authUser = await userAuthentication(req.headers.token)
      if (authUser) {
        if (req.body.user_id == '') {
          let allusers = await getUserList()
          if (allusers) {
            // res.json(allusers)
            var record = [];
            allusers.forEach(function (data3) {
              if (data3.profile_image == "") {
                var profile_image = baseurl;
              } else {
                var profile_image = profile_images + data3.profile_image;
              }
              record.push({
                "_id": data3._id,
                "user_name": data3.user_name,
                "email": data3.email,
                "phone_no": data3.phone_no,
                "city": data3.city,
                "state": data3.state,
                "exp_sponser_name": data3.exp_sponser_name,
                "licenced_years": data3.licenced_years,
                "profile_image": profile_image
              });
            });
            res.json({
              "status": "1",
              "status_message": "Agents Found",
              "All_agents": record,
            });
            console.log("all users")
          } else {
            console.log("no users")
            res.json({
              "status": "1",
              "status_message": "No Agents Found",
            });
          }
        } else {
          /*single user details*/
          const user = await getUser(req.body.user_id)
          /* sales details pounts */
          let salesPoints = await getSalePoint(user._id)
          sales = [];
          let sales_Point = 0;
          salesPoints.forEach(item => {
            sales_Point += item.activity_point;
          });
          sales.push({ sales_Point: sales_Point });
          /* activity points details */
          let activityPoints = await getActivityPoint(user._id)
          activity = [];
          let activity_Point = 0;
          activityPoints.forEach(item => {
            activity_Point += item.activity_point;
          });
          activity.push({ activity_Point: activity_Point });
          if (user.profile_image == "") {
            var profile_image = baseurl;
          } else {
            var profile_image = profile_images + user.profile_image;
          }
          res.json({
            "status": "1",
            "status_message": "Agent Details Found",
            "_id": user._id,
            "user_name": user.user_name,
            "email": user.email,
            "phone_no": user.phone_no,
            "city": user.city,
            "state": user.state,
            "exp_sponser_name": user.exp_sponser_name,
            "licenced_years": user.licenced_years,
            "profile_image": profile_image,
            "ban_status": user.status,
            "sales_points": sales,
            "activity_points": activity
          }); return;
        }
      } else {
        console.log('toekn mismatch')
        res.status(400).json({
          "status": "0",
          "message": "Invalid Token"
        });
      }
    }
  });
=======================
  router.post('/create_activityOld', upload.single('number_of_images'), (req, res, next) => {
    if (!req.headers.token) {
      res.json({
        "status": "0",
        "status_message": "Fields are required",
      }); return;
    }
    else {
      User.findOne({ 'token': req.headers.token }, function (err, data) {
        try {
          if (err) {
            res.json({
              "status": "0",
              "status_message": "Fields are required"
            }); return;
          } else {
            if (data) {
              if (data.token != req.headers.token) {
                res.json({
                  "status": "0",
                  "status_message": "User does not Exist.",
                }); return;
              } else {
                var user_id = data._id;
                var activity_id = req.body.activity_id;
                var activity_date = req.body.activity_date;
                var c_date = req.body.date;
                var m_date = req.body.date;

                var createdactivity = new CreatedActivity({
                  user_id: user_id,
                  activity_id: activity_id,
                  activity_date: activity_date,
                  created_date: c_date,
                  modified_date: m_date,
                  is_deleted: "0"
                });
                createdactivity.save()

                Activity.findOne({ '_id': req.body.activity_id, is_deleted: '0' }, function (err, data1) {
                  var points = data1.activity_points;
                  var type = data1.type;
                  console.log(points);
                  console.log("space");
                  console.log(type);

                  if (type == 'activity') {
                    User.findOneAndUpdate({ _id: data._id }, { $inc: { activitypoints: points } }).exec(function (err, db_res) {
                      if (err) {
                        throw err;
                      }
                      else {
                        console.log("points updated");
                      }
                    });
                  }else {
                    User.findOneAndUpdate({ _id: data._id }, { $inc: { salepoints: points } }).exec(function (err, db_res) {
                      if (err) {
                        throw err;
                      }
                      else {
                        console.log("points updated");
                      }
                    });
                  }

                  var activitypoint = new ActivityPoint({
                    user_id: user_id,
                    activity_id: activity_id,
                    activity_point: data1.activity_points,
                    type: data1.type,
                    created_date: c_date,
                    modified_date: m_date,
                    is_deleted: "0"
                  });
                  activitypoint.save()

                })
                  .then(result => {
                    res.status(200).json({
                      status: "1",
                      message: "Activity Created successfully",
                    });
                  })
                  .catch(err => {
                    res.status(500).json({
                      "Status": "0",
                      "message": "Error"
                    });
                  });
              }
            } else {
              res.json({
                "status": "0",
                "status_message": "User not Found",
              }); return;
            }
          }
        } catch (e) {
          res.json({ "status": "0", "status_message": "fghfh", 'error': e }); return;
        }
      });
    }
  });

  ==================

  activityId = req.body.id
  ActivityPoint.aggregate([
    { "$match": { "activity_id": ObjectId(activityId) } },
    { $group: { _id: { user_id: "$user_id" }, total: { $sum: "$activity_point" } } },
    {
      $lookup: {
        from: "users",
        localField: "_id.user_id",
        foreignField: "_id",
        as: "userData"
      }
    },
    {
      $unwind: "$userData"
    },
    {
      "$sort": { "total": -1 }
    },
  ], function (err, data) {
    if (data != '') {
      var topUsers = []
      var rank = 1
      data.forEach(function (element) {
        let user = {
          userId: element.userData._id,
          userName: element.userData.user_name,
          points: element.total,
          city: element.userData.city,
          state: element.userData.state,
          rank: rank
        }
        rank++
        topUsers.push(user)
      });
      res.json({
        "status": "1",
        "status_message": "Agents Found",
        "listing": topUsers,
      });
    } else {
      res.json({
        "status": "1",
        "status_message": "No Agents Found",
      });
    }
  })

  ======25

  router.post('/agenthistory2', upload.single('number_of_images'), async (req, res, next) => {
    if (!req.headers.token || !req.body.type || !req.body.user_id) {
      res.json({
        "status": "0",
        "status_message": "Fields are required",
      }); return;
    }
    else {
      let authUser = await userAuthentication(req.headers.token)
      if (authUser) {
      if (req.body.type == 'activity') {
        userId = req.body.user_id
        activity = 'activity'
        let act_history = await actHistory(userId,activity)
        if (act_history != '') {
          var topUsers = []
          var rank = 1
          act_history.forEach( async function (element) {
            /*agent rank*/
            let activityId = element._id.activity_id
            var act_history = await actRank(activityId)
            let topU = []
            var rankk = 1
            act_history.forEach(async function (element) {
              let userrank = {
                userId: element._id.user_id,
                points: element.total,
                rank: rankk
              }
              rankk++
              await topU.push(userrank)
            });
            // const filterArray = req.body.user_id
            // var result = topU.filter(({ userId }) => filterArray.includes(userId));
            // /*agent rank*/
            let user = {
              activity_id: element._id.activity_id,
              points: element.totalPoints,
              activity_name: element.activityData.activity_name.capitalize(),
              rank: rankk
            }
          await  topUsers.push(user)
          console.log(topUsers);
          })
          if(topUsers.length > 0){
            res.json({
               "status": "1",
               "status_message": "Activities History Found",
               "listing": topUsers,
             });
          }
        }else{
          res.json({
            "status": "1",
            "status_message": "No Data Found",
          });
        }
      }else if (req.body.type == 'sale'){
        userId = req.body.user_id
        sale = 'sale'
        let sale_history = await saleHistory(userId,sale)
        if (sale_history != '') {
          var topUsers = []
          var rank = 1
          sale_history.forEach( function (element) {
            let user = {
              activity_id: element._id.activity_id,
              points: element.totalPoints,
              activity_name: element.activityData.activity_name.capitalize(),
              rank: rank
            }
            rank++
            topUsers.push(user)
          })
          res.json({
            "status": "1",
            "status_message": "Sales History Found",
            "listing": topUsers,
          });
        }else{
          res.json({
            "status": "1",
            "status_message": "No Data Found",
          });
        }
      }
    } else {
        console.log('toekn mismatch')
        res.status(400).json({
          "status": "0",
          "message": "Invalid Token"
        });
      }
    }
  });


====
router.post('/search_agent', upload.single('image'), (req, res, next) => {
  if (!req.headers.token || !req.body.search) {
    res.json({
      "status": "0",
      "status_message": "Fields are required",
    }); return;
  }
  else {
    User.findOne({ 'token': req.headers.token }, function (err, data) {
      try {
        if (err) {
          res.json({
            "status": "0",
            "status_message": "Fields are required"
          }); return;
        } else {
          if (data) {
            if (data.token != req.headers.token) {
              res.json({
                "status": "0",
                "status_message": "User does not Exist.",
              }); return;
            } else {
              var search = req.body.search;
              User.find({
                $or: [
                  { 'user_name': new RegExp(search, 'i') },
                  { 'email': new RegExp(search, 'i') },
                  { 'phone_no': new RegExp(search, 'i') }
                ],
                $and: [
                  { _id: { $ne: data._id } }
                ]
              }, function (err, data) {
                if (data != '') {
                  var record = [];
                  data.forEach(function (data3) {
                    if (data3.profile_image == "") {
                      var profile_image = baseurl;
                    } else {
                      var profile_image = profile_images + data3.profile_image;
                    }
                    record.push({
                      "_id": data3._id,
                      "user_name": data3.user_name.capitalize(),
                      "email": data3.email,
                      "phone_no": data3.phone_no,
                      "city": data3.city.capitalize(),
                      "state": data3.state.capitalize(),
                      "exp_sponser_name": data3.exp_sponser_name.capitalize(),
                      "licenced_years": data3.licenced_years,
                      "profile_image": profile_image
                    });
                  });
                  res.json({
                    "status": "1",
                    "status_message": "Agents Found",
                    "listing": record,
                  });
                } else {
                  res.json({
                    "status": "1",
                    "status_message": "No Agent Found",
                  });
                }
              });
            }
          } else {
            res.json({
              "status": "0",
              "status_message": "Invalid User",
            }); return;
          }
        }
      } catch (e) {
        res.json({ "status": "0", "status_message": "Something went wrong", 'error': e }); return;
      }
    });
  }
});
===


  ======================


  router.post('/agenthistory1', upload.single('number_of_images'), async (req, res, next) => {
    if (!req.headers.token || !req.body.type || !req.body.user_id) {
      res.json({
        "status": "0",
        "status_message": "Fields are required",
      }); return;
    }
    else {
      let authUser = await userAuthentication(req.headers.token)
      if (authUser) {
      if (req.body.type == 'activity') {
        let userId = req.body.user_id
        let activity = 'activity'
        let topU = []
        let rank = 1
        var topUsers = []
        let actRes = await actHistory(userId,activity)
        actRes.forEach( function (actRess) {
          actRes.forEach(async(activityHst) => {
            let activityId = activityHst._id.activity_id
            let actRankRes  = await actRank(activityId)
            actRankRes.forEach(async(item) => {
              rank ++
              let user = {
                userId: item._id.user_id,
                points: item.total,
                rank: rank
              }
            await topU.push(user)
            });
            // console.log('topU',topU)
          });
          let user = {
            activity_id: actRess._id.activity_id,
            points: actRess.totalPoints,
            activity_name: actRess.activityData.activity_name.capitalize(),
            rank: topU
          }
          topUsers.push(user)
        })
        res.json({
          "status": "1",
          "status_message": "Activities History Found",
          "listing": topUsers,
        });

      }else if (req.body.type == 'sale'){
        userId = req.body.user_id
        sale = 'sale'
      }
    } else {
        console.log('toekn mismatch')
        res.status(400).json({
          "status": "0",
          "message": "Invalid Token"
        });
      }
    }
  });
