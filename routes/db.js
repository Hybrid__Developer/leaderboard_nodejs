var mongoose = require('mongoose');
/*Live database url*/
mongoose.connect('mongodb://93.188.167.68:33017/leaderboardDB');
/*Local database*/
// mongoose.connect('mongodb://localhost/leaderboardDB');
var con = mongoose.connection;
con.on('error', console.error.bind(console, 'connection error:'));
con.once('open', function callback () {
  console.log("connected");
  con.collection('leaderboardDB', function(err, collection) {
    console.log(collection);
  });
});

module.exports = con;
