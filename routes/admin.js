var express = require('express');
var con = require('./db');
var Promise = require('promise');
var formidable = require('formidable');
const mongoose = require('mongoose');
var body = require('body-parser');
var _ = require('underscore-node');
var Q = require("q");
// var FCM = require('fcm-node');
// var fcm = new FCM('AAAASGUfVDw:APA91bEW_8s4YNhx2XXHOs7EFIR8V_cJe2oYD3MRhQjtt3s5LSq6KhylVjnfPTJE8vZw8mvSaGPG3X-0OTXTDl1zVQQ_zJ1SVm8GLUQB5daVU8nEypiNJXR2VVn6PorQDvja4qTOwla0'); ///client server key
// var fcm2 = new FCM('AAAAzgOun5w:APA91bH5VC_dllp41Z9CwaEHSH4OAJlgZvDzQi2nMYypK_0l0wtGhFpAXRAgCwMMnlSkoSNmGVkFqVCG2AiiW2vyj5pgjCbBOkQmx-UjmSeFCX-hsuf6orsyFuEapMcgLLM4DlBh7xxN'); ///Photographer server key
var http = require('http');
var router = express.Router();
var nodemailer = require('nodemailer');
var Genpassword = require('secure-random-password');
var User = require("../models/user_model");
var Activity = require("../models/activity_model");
var CreatedActivity = require("../models/createdactivity_model");
var ActivityPoint = require("../models/activitypoint_model");
//var Notification = require("../models/notification");
var crypto = require('crypto');
var multer = require('multer');
var extend = require('node.extend');
var async = require("async");
var $ = require("jquery");
var fs = require('fs');
var path = require('path');
var multerS3 = require('multer-s3');
var unique = require('array-unique');
var str = require('node-strings');
const uniqueArrayBy = require('unique-array-by')
var md5 = require('md5');
var apn = require('apn');
var db;
const ObjectId = require("mongodb").ObjectID;

/*multer for storing originalname of files in database and also in storage*/
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/images/');
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});

var upload = multer({ storage: storage })
var upload_profile_image = multer({ storage: storage, limits: { fileSize: 1024 * 1024 * 50 } });
var upload_image = multer({ storage: storage, limits: { fileSize: 1024 * 1024 * 50 } });
var upload = multer({ storage: storage, limits: { fileSize: 1024 * 1024 * 50 } });
/*base url for getting images of a user --*/
// var profile_images = 'http://localhost/leaderboard_nodejs/uploads/images/';
// var baseurl = 'http://localhost/leaderboard_nodejs/uploads/default.png';
/*if user havnt a profile pic then it will be default picture*/
var profile_images = 'https://93.188.167.68/projects/leaderboard_nodejs/uploads/images/';
var baseurl = 'https://93.188.167.68/projects/leaderboard_nodejs/uploads/deafult.png';

  router.post('/banUnban_User', upload.single('number_of_images'), async (req, res, next) => {
    if (!req.headers.token || !req.body.user_id) {
      res.json({
        "status": "0",
        "status_message": "Fields are required",
      }); return;
    }
    User.findOne({ 'token': req.headers.token }, function (err, userData) {
      if (userData) {
        User.findOne({'_id':req.body.user_id},function (err, userDetail){
          if (userDetail) {
            if (userDetail.status == 'Unban') {
              var myquery = { _id: userDetail._id };
              var status = 'ban';
              var newvalues = { $set: { status: status } };
              User.updateOne(myquery, newvalues, function (err, result) {
                if (result) {
                  res.json({
                    "status": "1",
                    "status_message": "User banned Successfully"
                  });
                }else {
                  res.json({
                    "status": "0",
                    "status_message": "Error"
                  });
                }
              });
            }else if (userDetail.status == 'ban') {
              var myquery = { _id: userDetail._id };
              var status = 'Unban';
              var newvalues = { $set: { status: status } };
              User.updateOne(myquery, newvalues, function (err, result) {
                if (result) {
                  res.json({
                    "status": "1",
                    "status_message": "User Unbanned Successfully"
                  });
                }else {
                  res.json({
                    "status": "0",
                    "status_message": "Error"
                  });
                }
              });
            }
          }else {
            res.json({
              "status": "0",
              "status_message": "User not found"
            });
          }
        })
      }
      else {
        res.json({
          "status": "0",
          "status_message": "Invalid Token",
        }); return;
      }
    });
  })

  let userAuthentication = (token) => {
    return new Promise((resolve, reject) => {
      User.findOne({ 'token': token }).then(response => {
        return resolve(response);
      }).catch(error => {
        return reject(error);
      });

    })
  }

  let getDeletedActivityPoint = () => {
    return new Promise((resolve, reject) => {
      ActivityPoint.remove({}).then(response => {
        return resolve(response);
      }).catch(error => {
        return reject(error);
      });
    })
  }

  let getDeletedCreatedActivity = () => {
    return new Promise((resolve, reject) => {
      CreatedActivity.remove({}).then(response => {
        return resolve(response);
      }).catch(error => {
        return reject(error);
      });
    })
  }

  let resetUsersPoints = () => {
    return new Promise((resolve, reject) => {
      User.updateMany({}, {$set: {activitypoints: "0",salepoints: "0"}}).then(response => {
        return resolve(response);
      }).catch(error => {
        return reject(error);
      });
    })
  }

  router.post('/trashAlldata', upload.single('number_of_images'), async (req, res, next) => {
    if (!req.headers.token) {
      res.json({
        "status": "0",
        "status_message": "Fields are required",
      }); return;
    }
    let authUser = await userAuthentication(req.headers.token)
    if (authUser) {

      let trashdata = await getDeletedActivityPoint()
      let trashdataNew = await getDeletedCreatedActivity()
      let resetPoints = await resetUsersPoints()
      if (resetPoints) {
        res.json({
          "status": "1",
          "status_message": "Data Deleted",
        }); return;
      }else {
        res.json({
          "status": "1",
          "status_message": "There is no data",
        }); return;
      }
    } else {
      console.log('toekn mismatch')
      res.status(400).json({
        "status": "0",
        "message": "Invalid Token"
      });
    }
  })

module.exports = router;
