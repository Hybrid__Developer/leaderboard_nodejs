var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var userSchema = new Schema({
	user_name:{
		type:String
	},
	city:{
		type:String,
		default: ""
	},
	phone_no:{
		type:String,
		default: ""
	},
	user_type:{
		type:String,
		default: "U"
	},
	status:{
		type:String,
		default: "Unban"
	},
	state:{
		type:String,
		default: ""
	},
	exp_sponser_name:{
		type:String,
		default: ""
	},
	licenced_years:{
		type:String,
		default: ""
	},
	phone_number: {
		type: String,
		maxlength: 15,
		default: ""
	},
	device_id:{
		type: String,
	},
	device_type:{
		type: String,
	},
	email: {
		type: String,
		sparse: true,
		unique:true,
		maxlength: 255
	},
	password: {
		type: String,
		default:"",
	},
	created_date: {
		type: Date,
		default: Date.now
	},
	modified_date: {
		type: Date,
		default: Date.now
	},
	is_deleted:{
		type: Boolean,
		default: 1
	},
	token:{
		type: String,
		maxlength: 225
	},
	activitypoints:{
		type: Number,
		default: 0
	},
	salepoints:{
		type: Number,
		default: 0
	},
	badges_points:{
		type: Number,
		default: 0
	},
	social_id:{
		type:String
	},
	profile_status:{
		type: String,
		default:"0", // '0' for no and '1' for yes 
	},
	profile_image:{
		type:String,
		default: "default.png"
	}
},
{
   versionKey: false // You should be aware of the outcome after set to false
});
userSchema.plugin(uniqueValidator,{ message: '{PATH} is to be unique.' });
var user = mongoose.model('user', userSchema);
module.exports = user;
