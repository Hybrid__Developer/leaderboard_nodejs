var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var createdactivitySchema_ = new Schema({

    activity_id: {
        type: String,
    },
		user_id: {
        type: String,
    },
		activity_date: {
        type: String,
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    modified_date: {
        type: Date,
        default: Date.now
    },
    is_deleted:{
        type: Boolean,
        default: 1
    }
},
{
   versionKey: false // You should be aware of the outcome after set to false
});
createdactivitySchema_.plugin(uniqueValidator,{ message: '{PATH} is to be unique.' });
var createdactivity = mongoose.model('createdactivity', createdactivitySchema_);
module.exports = createdactivity;
