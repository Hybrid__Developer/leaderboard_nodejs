var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var activitySchema_ = new Schema({

    activity_name: {
        type: String,
    },
		activity_description: {
        type: String,
    },
		type: {
        type: String,
    },
		activity_points: {
        type: String,
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    modified_date: {
        type: Date,
        default: Date.now
    },
    is_deleted:{
        type: Boolean,
        default: 1
    }
},
{
   versionKey: false // You should be aware of the outcome after set to false
});
activitySchema_.plugin(uniqueValidator,{ message: '{PATH} is to be unique.' });
var activity = mongoose.model('activity', activitySchema_);
module.exports = activity;
