var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;

var activitypointSchema_ = new Schema({

    activity_id: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "activity"
    },
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user"
    },
		activity_point: {
        type: Number,
    },
		type: {
        type: String,
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    modified_date: {
        type: Date,
        default: Date.now
    },
    is_deleted:{
        type: Boolean,
        default: 1
    }
},
{
   versionKey: false // You should be aware of the outcome after set to false
});
activitypointSchema_.plugin(uniqueValidator,{ message: '{PATH} is to be unique.' });
var activitypoint = mongoose.model('activitypoint', activitypointSchema_);
module.exports = activitypoint;
