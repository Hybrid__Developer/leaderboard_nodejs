var express = require('express');
var app = express();
var user = require('./routes/user');
var admin = require('./routes/admin');
var http = require('http');
var con = require('./routes/db');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var fileUpload = require('express-fileupload');

// app.use(bodyParser.json());
var server = app.listen(1512, function(){
  console.log(new Date().toISOString() + ": server started on port 1512");
});
app.use(bodyParser.json({limit:'50mb'}));
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb', parameterLimit: 500000000 }));
app.use(bodyParser.json());
app.use('/user', user);
app.use('/admin', admin);

var allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', ['GET,PUT,POST,DELETE', 'OPTIONS']);
  res.header('Access-Control-Allow-Headers', '*');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  next();
}

String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}
module.exports = app;
